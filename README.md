# Pringting App

| gulp command                | shortcut             | what it does                                                                                   |
|-----------------------------|----------------------|------------------------------------------------------------------------------------------------|
| `gulp`                      | —                    | run local development server, start watchers, auto reload browser on change, targetfolder /tmp |
| `gulp --build`              | `gulp -b`            | create a build from current `/app` folder, minify assets, targetfolder `/www`                  |
| `gulp --emulate <platform>` | `gulp -e <platform>` | run a build first, then ionic emulate <platform>. defaults to ios                              |
| `gulp --run <platform>`     | `gulp -r <platform>` | run a build first, then ionic run <platform>. defaults to ios                                  |
| `gulp test-unit`            | none                 | run all the test cases under `test/unit` folder using Karma runner                             |
| `gulp test-e2e`             | none                 | run all the test cases under `test/e2e` folder using Protractor      

Add below filter into AndroidManifest.xml

            <intent-filter>
                <action android:name="android.intent.action.SEND" />
                <action android:name="android.intent.action.SEND_MULTIPLE" />
                <category android:name="android.intent.category.DEFAULT" />
                <data android:mimeType="image/*" />
                <data android:mimeType="text/plain" />
                <data android:mimeType="application/msword" />
                <data android:mimeType="application/vnd.openxmlformats-officedocument.wordprocessingml.document" />
                <data android:mimeType="application/pdf" />
            </intent-filter>


(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name printingApp
     * @description
     * # Initializes main application and routing
     *
     * Main module of the application.
     */


    angular.module('printingApp', ['ionic', 'ionic-material', 'ngCordova', 'ngResource', 'ngSanitize', 'PubSub', 'pascalprecht.translate'])

    .run(function($ionicPlatform, $rootScope) {

        $rootScope.$on('$translateChangeSuccess', function() {
            console.log('Selected language applied');
        });

        $rootScope.$on('$translateChangeError', function() {
            alert('Error, selected language was not applied');
        });

        $ionicPlatform.ready(function() {
            // save to use plugins here
        });

        // add possible global event handlers here

    })

    .config(['$httpProvider', '$stateProvider', '$urlRouterProvider', '$compileProvider', '$ionicConfigProvider', '$translateProvider', function($httpProvider, $stateProvider, $urlRouterProvider, $compileProvider, $ionicConfigProvider, $translateProvider) {
        $ionicConfigProvider.backButton.previousTitleText(false);
        $ionicConfigProvider.backButton.icon('ion-chevron-left');
        $ionicConfigProvider.backButton.text('');
        // register $http interceptors, if any. e.g.
        // $httpProvider.interceptors.push('interceptor-name');

        if (localStorage.lang) {
            $translateProvider.preferredLanguage(localStorage.lang);
        } else {
            $translateProvider.preferredLanguage('en');
        }

        $translateProvider.registerAvailableLanguageKeys(['en', 'fr'], {
            'en-*': 'en',
            'fr-*': 'fr'
        });

        $translateProvider.useStaticFilesLoader({
            prefix: 'scripts/data/locale-',
            suffix: '.json'
        });

        $translateProvider.useSanitizeValueStrategy(null);


        // Application routing
        $stateProvider
            .state('login', {
                url: '/login',
                cache: false,
                templateUrl: 'templates/views/login.html',
                controller: 'LoginController as vm',

            })

        .state('app', {
            url: '/app',
            abstract: true,
            controller: 'MainController as vm',
            templateUrl: 'templates/main.html'
        })

        .state('app.sharing', {
            url: '/sharing',
            cache: false,
            views: {
                'viewContent': {
                    templateUrl: 'templates/views/shareFile.html',
                    controller: 'sharingController as vm',
                }
            }
        })

        .state('app.myfiles', {
            url: '/myfiles',
            cache: false,
            views: {
                'viewContent': {
                    templateUrl: 'templates/views/myFiles.html',
                    controller: 'myFilesController as vm'
                }
            }
        })


        .state('app.credit', {
            url: '/credit',
            cache: true,
            views: {
                'viewContent': {
                    templateUrl: 'templates/views/credit.html',
                    controller: 'CreditController as vm'
                }
            }
        })

        .state('app.settings', {
            url: '/settings',
            cache: true,
            views: {
                'viewContent': {
                    templateUrl: 'templates/views/settings.html',
                    controller: 'SettingsController as vm'
                }
            }
        })

        .state('app.home', {
            url: '/home',
            cache: true,
            views: {
                'viewContent': {
                    templateUrl: 'templates/views/home.html',
                    controller: 'homeController as vm'
                }
            }
        });

        // redirects to default route for undefined routes
        $urlRouterProvider.otherwise('/login');
    }])
})();
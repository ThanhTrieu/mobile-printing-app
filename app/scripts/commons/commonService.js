(function() {
    'use strict';
    angular.module('printingApp')
        .factory('CommonService', CommonService);

    CommonService.$inject = ['$q', '$http', '$window'];

    function CommonService($q, $http, $window) {
        var service = {
            getDataFromFile: getDataFromFile,
            isUrl: isUrl,
            getFileContentAsBase64: getFileContentAsBase64
        }

        return service;
        ///////////////////////////


        function getDataFromFile(filename) {
            var deferred = $q.defer();
            $http.get("resources/" + filename)
                .then(function(data, status, headers, config) {
                    deferred.resolve(data);
                }, function(data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }

        function isUrl(s) {
            var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
            return regexp.test(s);
        }

        function getFileContentAsBase64(path, callback) {
            window.resolveLocalFileSystemURL(path, gotFile, fail);

            function fail(e) {
                alert('Cannot found requested file');
            }

            function gotFile(fileEntry) {
                fileEntry.file(function(file) {
                    var reader = new FileReader();
                    reader.onloadend = function(e) {
                        var content = this.result;
                        callback(content);
                    };
                    reader.readAsArrayBuffer(file);
                });
            }
        }


    }
})();
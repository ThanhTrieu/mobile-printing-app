(function() {
    'use strict';

    /**
     * @ngdoc constant
     * @name printingApp.API_ENDPOINT
     * @description
     * # API_ENDPOINT
     * Defines the API endpoint where our resources will make requests against.
     * Is used inside /services/ApiService.js to generate correct endpoint dynamically
     */


    angular.module('printingApp')

    // development
    .constant('API_ENDPOINT', 'https://copyshop.myworkplatform.biz/api/winapp/')
        .constant('API_DOWNFILE', 'https://copyshop.myworkplatform.biz/files/')
        .run(['$rootScope', function($rootScope) {
            $rootScope._ = window._;
        }]);


    // live example with HTTP Basic Auth
    /*
    .constant('API_ENDPOINT', {
        host: 'http://myserver.com',
        path: '/api/v2',
        needsAuth: true,
        username: 'whatever',
        password: 'foobar'
    });
    */
})();
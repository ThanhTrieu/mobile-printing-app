(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name printingApp.controller:CreditController
     * @description
     * # CreditController
     */
    angular.module('printingApp')
        .controller('CreditController', CreditController);
    CreditController.$inject = ['$scope', '$rootScope', 'ApiService', '$ionicLoading', '$translate'];

    function CreditController($scope, $rootScope, ApiService, $ionicLoading, $translate) {
        var vm = this;

        function init() {
            var loginUser = localStorage.loginUser;
            if (loginUser) {
                loginUser = JSON.parse(loginUser);
                var _param = { "username": loginUser.userName };
                var token = localStorage.tokenuserID;
                $ionicLoading.show();
                if (token) {
                    ApiService.callApi("credits_history/" + token, "POST", _param)
                        .then(function(data) {
                            vm.credits = JSON.parse(data.history);
                            vm.credits = _.orderBy(vm.credits, ['datetime'], ['desc']);
                            vm.myCredit = vm.credits[0].solde;
                            $ionicLoading.hide();
                        }, function(err) {
                            console.log(err);
                            $ionicLoading.hide();
                        });
                }
            }
        }

        $scope.$on('$ionicView.beforeEnter', function() {
            init();
        });
    }
})();
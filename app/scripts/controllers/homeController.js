(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name printingApp.controller:homeController
     * @description
     * # homeController
     */
    angular.module('printingApp')
        .controller('homeController', homeController);
    homeController.$inject = ['$scope', '$rootScope', 'ApiService', '$ionicLoading', 'API_DOWNFILE', '$timeout', 'CommonService', '$translate'];

    function homeController($scope, $rootScope, ApiService, $ionicLoading, API_DOWNFILE, $timeout, CommonService, $translate) {
        var vm = this;
        vm.files = [];
        vm.isLoaded = true;
        vm.AdvImageSource = localStorage.AdvImageSource;
        vm.openFile = openFile;
        vm.loadFiles = loadFiles;
        vm.scanBarcode = scanBarcode;
        vm.onRefresh = onRefresh;

        function scanBarcode() {
            cordova.plugins.barcodeScanner.scan(
                function(result) {
                    if (!result.cancelled) {
                        var token = localStorage.tokenuserID;
                        var param = { "tokenuserID": token, "printerID": result.text };
                        ApiService.callApi('printersession', 'POST', param).then(function(data) {
                            alert('printersessionID: ' + data.printersessionID);
                        });
                    }
                },
                function(error) {
                    alert("Scanning failed: " + error);
                }
            );
        }

        function loadFiles() {
            vm.files.length = 0;
            vm.isLoaded = false;
            var token = localStorage.tokenuserID;
            if (token) {
                $ionicLoading.show();
                ApiService.callApi('dirlist/' + token, 'POST', { 'tokenuserID': token })
                    .then(function(data) {
                        if (data != 'null') {
                            _.forEach(data, function(file) {
                                file.url = API_DOWNFILE + file.name;
                                file.type = file.name.substring(file.name.lastIndexOf('.') + 1);
                            });
                            vm.files = data;
                        }
                        $timeout(function() {
                            vm.isLoaded = true;
                            $scope.$broadcast('scroll.refreshComplete');
                            $ionicLoading.hide();
                        }, 500);
                    }, function(err) {
                        console.log('err :', err);
                        $scope.$broadcast('scroll.refreshComplete');
                        $ionicLoading.hide();
                    })
            }
        }

        function loadCredits() {
            var loginUser = localStorage.loginUser;
            if (loginUser) {
                loginUser = JSON.parse(localStorage.loginUser);
                var _param = { "username": loginUser.userName };
                var token = localStorage.tokenuserID;
                if (token) {
                    ApiService.callApi("credits_history/" + token, "POST", _param)
                        .then(function(data) {
                            vm.credits = JSON.parse(data.history);
                            vm.credits = _.orderBy(vm.credits, ['datetime'], ['desc']);
                            vm.myCredit = vm.credits[0] ? vm.credits[0].solde : '';
                        }, function(err) {
                            console.log(err);
                        });
                }
            }
        }

        function openFile(url) {
            window.open(url, '_system', 'location=no');
        }

        function onRefresh() {
            loadFiles();
            loadCredits();
        }
        $scope.$on('$ionicView.beforeEnter', function() {
            loadFiles();
            loadCredits();
        });
    }
})();
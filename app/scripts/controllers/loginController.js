(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name printingApp.controller:LoginController
     * @description
     * # LoginController
     */
    angular.module('printingApp')
        .controller('LoginController', LoginController)
    LoginController.$inject = ['$scope', '$rootScope', '$state', 'CommonService', 'ApiService', '$timeout', '$interval', '$ionicPopup', '$ionicLoading', '$translate', 'facebookAuthService'];

    function LoginController($scope, $rootScope, $state, CommonService, ApiService, $timeout, $interval, $ionicPopup, $ionicLoading, $translate, facebookAuthService) {
        var vm = this;
        vm.email = localStorage.email;
        vm.passWord = '';
        vm.login = login;
        vm.incorrect = false;
        vm.hasIntent = false;
        // vm.isDeviceReady = false;
        vm.isDeviceReady = true;
        $rootScope.intent = '';
        vm.googleLogin = googleLogin;
        vm.faceBookLogin = faceBookLogin;

        function init() {
            var loginUser = localStorage.loginUser;
            if (!_.isUndefined(loginUser)) {
                $state.go('app.home');
                clearToken();
            }
        }


        function login(_email, _passWord, _methob, _accessToken) {
            $ionicLoading.show();
            var _param = { 'username': _email, 'password': _passWord, 'method': _methob, 'accessToken': _accessToken };
            localStorage.email = _email;
            _param = JSON.stringify(_param);
            vm.incorrect = false;
            ApiService.getAUTH(_param).then(function(response) {
                if (response.tokenuserID) {
                    localStorage.tokenuserID = response.tokenuserID;
                    localStorage.loginUser = JSON.stringify(_param);
                    clearToken();
                    if (vm.hasIntent) {
                        $state.go('app.sharing');
                    } else {
                        $state.go('app.home');
                    }
                    vm.hasIntent = false;
                } else {
                    vm.incorrect = true;
                }
                $ionicLoading.hide();
            })
        }

        var start;

        function clearToken() {
            if (!start) {
                start = $timeout(function() {
                    localStorage.removeItem('tokenuserID');
                    localStorage.removeItem('loginUser');
                    $timeout.cancel(start);
                    start = undefined;
                    loginAgain();
                }, 60000 * 60);
            }
        }

        function loginAgain() {
            var alertPopup = $ionicPopup.alert({
                title: 'TokenUserId is expired',
                cssClass: 'my-custom-popup',
                template: '<p>You can not intergate with server without TokenUserID.</p><p>Please login again to get a new one!</p>',
                okType: 'button-orange',
            });

            alertPopup.then(function(res) {
                // $state.go('login');
            });
        }

        document.addEventListener('deviceReady', function() {
            vm.isDeviceReady = true;
            try {
                window.plugins.intent.getCordovaIntent(function(Intent) {
                    if (!_.isUndefined(Intent.clipItems)) {
                        var loginUser = localStorage.loginUser;
                        $rootScope.shareFile = Intent.clipItems;
                        console.log($rootScope.shareFile);
                        if (!_.isUndefined(loginUser)) {
                            $state.go('app.sharing');
                            clearToken();
                        } else {
                            vm.hasIntent = true;
                        }
                    } else {
                        init();
                    }
                }, function() {
                    console.log('Error');
                });
            } catch (error) {

            }
        }, true);

        function getAvdImage() {
            ApiService.callApi('adv/mobile/rWZbnFxGV76k29Njt4KdBzgfqpYJLC')
                .then(function(data) {
                    vm.AdvImageSource = data.urlimage;
                    localStorage.AdvImageSource = data.urlimage;
                })
        }
        getAvdImage();

        function googleLogin() {
            window.plugins.googleplus.login({
                    'webClientId': '595919054098-0iafdo2iik8ncvhgf2phih7e827fe7ed.apps.googleusercontent.com'
                },
                function(obj) {
                    //alert(JSON.stringify(obj));
                    login(obj.email, "123456", "google", obj.idToken);
                },
                function(msg) {
                    alert('error: ' + msg);
                }
            );
        }

        function faceBookLogin() {
            facebookAuthService.loginFacebook().then(function(data) {
                if (data.status == 'connected') {
                    var accessToken = data.authResponse.accessToken;
                    facebookAuthService.getFacebookApi()
                        .then(
                            function(userInfo) {
                                login(userInfo.email, "123456", "google", accessToken);
                            },
                            function(err) {
                                console.log(err);
                            }
                        );
                }
            }, function(err) {
                console.log(err);
            });
            // CordovaFacebook.login({
            //     permissions: ['email', 'user_likes'],
            //     onSuccess: function(result) {
            //         if (result.declined.length > 0) {
            //             alert("The User declined something!");
            //         } else {
            //             console.log(result);
            //             CordovaFacebook.profile({
            //                 permissions: ['email', 'user_likes'],
            //                 onSuccess: function(userInfo) {
            //                     console.log("user " + userInfo);
            //                 },
            //                 onFailure: function(err) {
            //                     console.log("err " + err);
            //                 }
            //             });
            //             CordovaFacebook.logout({
            //                 onSuccess: function() {
            //                     alert("The user is now logged out");
            //                 }
            //             });
            //         }
            //     },
            //     onFailure: function(result) {
            //         if (result.cancelled) {
            //             alert("The user doesn't like my app");
            //         } else if (result.error) {
            //             alert("There was an error:" + result.errorLocalized);
            //         }
            //     }
            // });
        }

        $scope.$on('$ionicView.beforeEnter', function() {
            if (vm.isDeviceReady) {
                init();
            }

        });
    }
})();
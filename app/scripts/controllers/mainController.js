(function() {
    'use strict';
    /**
     * @ngdoc function
     * @name printingApp.controller:MainController
     * @description
     * # MainController
     * This controller handles the side menu
     */
    angular.module('printingApp')
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$cordovaBarcodeScanner', 'CommonService', '$timeout', 'ApiService', '$window', '$state', '$translate'];

    function MainController($scope, $cordovaBarcodeScanner, CommonService, $timeout, ApiService, $window, $state, $translate) {
        var vm = this;
        vm.stop = stop;
        vm.isRecoring = false;
        vm.isVoiceRecorded = false;
        vm.recordVoice = recordVoice;
        vm.playback = playback;
        vm.scanBarcode = scanBarcode;
        vm.isPlaying = false;
        vm.logOut = logOut;

        function scanBarcode() {
            $cordovaBarcodeScanner
                .scan()
                .then(function(result) {
                    if (!result.cancelled) {
                        // if (CommonService.isUrl(result.text)) {
                        //     window.open(result.text, '_system', 'location=yes');
                        // } else {
                        //     //  alert("We got a barcode\n" +
                        //     // "Result: " + result.text + "\n" +
                        //     // "Format: " + result.format + "\n" +
                        //     // "Cancelled: " + result.cancelled);
                        //     alert(result.text + ' is not a URL');
                        // }
                        var token = localStorage.tokenuserID;
                        if (token) {
                            var param = { "tokenuserID": token, "printerID": result.text };
                            ApiService.callApi('printersession', 'POST', param).then(function(data) {
                                alert('printersessionID: ' + data.printersessionID);
                            });
                        }
                    }
                }, function(error) {
                    alert('Scanning failed: ' + error);
                });
        }

        var stopTimeOut;

        function recordVoice() {
            if (!vm.isPlaying) {
                if (!vm.isRecoring) {
                    vm.isRecoring = true;
                    record();
                    stopTimeOut = $timeout(function() {
                        recordVoice();
                    }, 180000);
                } else {
                    if (angular.isDefined(stopTimeOut)) {
                        $timeout.cancel(stopTimeOut);
                        stopTimeOut = undefined;
                    }
                    vm.isRecoring = false;
                    stop();
                }
            }
        }

        function stop() {
            window.plugins.audioRecorderAPI.stop(function(msg) {
                var token = localStorage.tokenuserID;
                ApiService.uploadVoice(msg, 'voice/' + token);

                $scope.$apply(function() {
                    vm.isVoiceRecorded = true;
                });
            }, function(msg) {
                // failed 
                alert('ko: ' + msg);
            });
        }

        function record() {
            cordova.plugins.diagnostic.requestMicrophoneAuthorization(function(status) {
                if (status === cordova.plugins.diagnostic.permissionStatus.GRANTED) {
                    window.plugins.audioRecorderAPI.record(function(savedFilePath) {
                        alert('ok: ' + msg);
                    }, function(msg) {
                        alert('ko: ' + msg);
                    }, 180); // record 3 minutes 
                }
            }, function(error) {
                console.error(error);
            });

        }

        function playback() {
            if (!vm.isRecoring && !vm.isPlaying && vm.isVoiceRecorded) {
                vm.isPlaying = true;
                window.plugins.audioRecorderAPI.playback(function(msg) {
                    alert('ok: ' + msg);
                    $scope.$apply(function() {
                        vm.isPlaying = false;
                    });
                }, function(msg) {
                    alert('ko: ' + msg);
                });
            }
        }

        function logOut() {
            // $window.localStorage.clear();
            localStorage.removeItem('loginUser');
            $state.go('login');
        }

    }
})();
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name printingApp.controller:myFilesController
     * @description
     * # myFilesController
     */
    angular.module('printingApp')
        .controller('myFilesController', myFilesController);

    myFilesController.$inject = ['$scope', 'ApiService', '$ionicLoading', 'API_DOWNFILE', 'PubSub', '$timeout', '$translate'];

    function myFilesController($scope, ApiService, $ionicLoading, API_DOWNFILE, PubSub, $timeout, $translate) {
        var vm = this;
        vm.files = [];
        vm.isLoaded = true;
        vm.openFile = openFile;
        vm.init = init;

        function init() {
            var token = localStorage.tokenuserID;
            if (token) {
                vm.files.length = 0;
                vm.isLoaded = false;
                $ionicLoading.show();
                ApiService.callApi('dirlist/' + token, 'POST', { 'tokenuserID': token })
                    .then(function(data) {
                        if (data != 'null') {
                            _.forEach(data, function(file) {
                                file.url = API_DOWNFILE + file.name;
                                file.type = file.name.substring(file.name.lastIndexOf('.') + 1);
                            });
                            vm.files = data;
                        }
                        $timeout(function() {
                            vm.isLoaded = true;
                            $scope.$broadcast('scroll.refreshComplete');
                            $ionicLoading.hide();
                        }, 500);
                    }, function(err) {
                        console.log('err :', err);
                        $scope.$broadcast('scroll.refreshComplete');
                        $ionicLoading.hide();
                    })
            }
        }

        function openFile(url) {
            window.open(url, '_system', 'location=no');
        }

        init();

        PubSub.subscribe('updateData', init);

        $scope.$on('$destroy', function() {
            PubSub.unsubscribe('updateData');
        });
    }
})();
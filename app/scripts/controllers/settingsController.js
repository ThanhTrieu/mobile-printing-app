(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name printingApp.controller:SettingsController
     * @description
     * # SettingsController
     */
    angular.module('printingApp')
        .controller('SettingsController', SettingsController);
    SettingsController.$inject = ['$scope', '$translate'];

    function SettingsController($scope, $translate) {
        var vm = this;
        vm.lang = 'en';
        vm.changeLanguage = changeLanguage;

        function changeLanguage(lang) {
            $translate.use(lang);
            localStorage.lang = lang;
        }

    }
})();
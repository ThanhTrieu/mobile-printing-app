(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name printingApp.controller:sharingController
     * @description
     * # sharingController
     */
    angular.module('printingApp')
        .controller('sharingController', sharingController);

    sharingController.$inject = ['$scope', '$state', '$rootScope', 'ApiService', '$ionicLoading', 'PubSub', '$translate', 'CommonService'];

    function sharingController($scope, $state, $rootScope, ApiService, $ionicLoading, PubSub, $translate, CommonService) {
        var vm = this;
        vm.src, vm.path;
        vm.clicked = false;
        vm.upLoadFile = upLoadFile;

        function init() {
            vm.isImage = false;
            if ($rootScope.shareFile) {
                var shareFile = $rootScope.shareFile[0];
                vm.sharedFileType = shareFile.extension;
                vm.isImage = _.includes(shareFile.type.toLowerCase(), 'image');
                cordova.plugins.diagnostic.requestExternalStorageAuthorization(function(status) {
                    if (status === cordova.plugins.diagnostic.permissionStatus.GRANTED) {
                        window.FilePath.resolveNativePath(shareFile.uri, function(result) {
                            vm.filePath = result;
                            vm.fileName = vm.filePath.substr(vm.filePath.lastIndexOf('/') + 1);
                            // Convert to binary
                            CommonService.getFileContentAsBase64(vm.filePath, function(base64Data) {
                                vm.shareBinary = base64Data;
                            });
                        }, function(error) {
                            alert("can not get file");
                        });
                    }
                }, function(error) {
                    console.error(error);
                });
            }
        }

        function upLoadFile() {
            vm.clicked = true;
            //send file from choosing
            if (vm.uploadFileInfo && vm.fileBibary) {
                var type = vm.uploadFileInfo.type.substring(_.lastIndexOf(vm.uploadFileInfo.type, '/') + 1, vm.uploadFileInfo.length, type);
                switch (type) {
                    case 'msword':
                        type = 'doc';
                        break;
                    case 'vnd.openxmlformats-officedocument.wordprocessingml.document':
                        type = 'docx';
                        break;
                }
                sendFile(type, vm.fileBibary);
            } else {
                if (vm.shareBinary) {
                    //send file from sharing
                    sendFile(vm.sharedFileType, vm.shareBinary);
                } else {
                    alert('Please choose a file to upload!');
                    vm.clicked = false;
                }
            }
        }

        function sendFile(type, binary) {
            var token = localStorage.tokenuserID;
            if (token) {
                $ionicLoading.show();
                ApiService.uploadFileToUrl('upload/' + type + '/' + token, binary)
                    .then(function(response) {
                        alert(JSON.stringify(angular.toJson(response)));
                        vm.clicked = false;
                        $ionicLoading.hide();
                        PubSub.publish('updateData', null);
                        $state.go('app.myfiles');
                    }, function(err) {
                        alert('Err: ' + err);
                        vm.clicked = false;
                        $ionicLoading.hide();
                    })
            }
        }

        $scope.$watch(angular.bind(this, function(fileBibary) {
            return this.fileBibary;
        }), function(newVal, oldVal) {
            if (vm.uploadFileInfo) {
                vm.isImage = false;
                vm.fileName = vm.uploadFileInfo.name;
                // vm.filePath = vm.fileBibary;
                // vm.isImage = _.includes(vm.uploadFileInfo.type, 'image/');
            }

        });

        $scope.$on('$ionicView.beforeEnter', function() {
            init();
        });

        $scope.$on('$destroy', function() {
            $rootScope.shareFile = undefined;
        });
    }
})();
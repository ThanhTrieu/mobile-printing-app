'use strict';
angular
    .module('printingApp')
    .directive('fileread', fileRead);

function fileRead() {
    return {
        scope: {
            fileread: '=',
            fileinfo: '='
        },
        link: linkFuc,
        controller: ReadFileController
    };

    function linkFuc(scope, element, attributes) {
        element.bind('change', function(changeEvent) {
            var validFile = checkfile(changeEvent.target.files[0].name);
            if (validFile) {
                var reader = new FileReader();
                scope.fileinfo = changeEvent.target.files[0];
                reader.onload = function(loadEvent) {
                    scope.$apply(function() {
                        scope.fileread = loadEvent.target.result;
                    });
                };
                reader.readAsArrayBuffer(changeEvent.target.files[0]);
            }
        });
    }

    function checkfile(sender) {
        var validExts = new Array('.doc', '.docx', '.pdf', '.png', '.jpg', '.jpeg', '.gif');
        var fileExt = sender.substring(sender.lastIndexOf('.'));
        if (validExts.indexOf(_.toLower(fileExt)) < 0) {
            alert('Invalid file selected, valid files are of ' +
                validExts.toString() + ' types.');
            return false;
        } else return true;
    }

}

ReadFileController.$inject = ['$scope']

function ReadFileController($scope) {

}
(function() {
    'use strict';
    /**
     * @ngdoc service
     * @name printingApp.ApiService
     * @description
     * # ApiService
     * Retrieves correct api to make requests against.
     * Uses settings from API_ENDPOINT defined in /config/apiEndpoint.js
     *
     * Usage example: $http({
     *                      url: ApiService.getEndPoint() + '/things',
     *                      method: 'GET'
     *                 })
     *
     */
    angular.module('printingApp')

    .factory('ApiService', ApiService);
    ApiService.$inject = ['$q', '$window', '$http', 'API_ENDPOINT', 'API_DOWNFILE'];

    function ApiService($q, $window, $http, API_ENDPOINT, API_DOWNFILE) {
        var service = {
            getAUTH: getAUTH,
            callApi: callApi,
            downLoadFile: downLoadFile,
            uploadFileToUrl: uploadFileToUrl,
            uploadVoice: uploadVoice
        }

        return service;

        /////////////

        function getAUTH(user) {
            var deferred = $q.defer();
            var config = {
                method: "POST",
                data: user,
                url: API_ENDPOINT + 'auth',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            $http(config).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.reject(response.status);
            });
            return deferred.promise;

        }

        function callApi(_url, _method, _params) {
            var deferred = $q.defer();
            var config = {
                method: _method || 'GET',
                data: _params || null,
                crossOrigin: true,
                url: API_ENDPOINT + _url,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            $http(config).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.reject(response.status);
            });
            return deferred.promise;
        }

        function uploadFileToUrl(_url, _params) {
            var deferred = $q.defer();
            var config = {
                method: 'POST',
                data: new Uint8Array(_params) || null,
                crossOrigin: true,
                url: API_ENDPOINT + _url,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: []
            };
            $http(config).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.reject(response.status);
            });
            return deferred.promise;
        }

        function uploadVoice(fileURL, _url) {
            var uri = encodeURI(API_ENDPOINT + _url);
            var options = new FileUploadOptions();

            options.fileKey = "file";
            options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
            options.mimeType = "audio/mp4";

            var headers = { 'headerParam': 'headerValue' };
            options.headers = headers;

            var ft = new FileTransfer();

            ft.upload(fileURL, uri, onSuccess, onError, options);

            function onSuccess(r) {
                alert("Upload voice successfully!");
            }

            function onError(error) {
                alert("An error has occurred: Code = " + error.code);
            }

        }

        function downLoadFile(_name, _params) {
            var deferred = $q.defer();
            var config = {
                method: 'POST',
                data: _params || null,
                crossOrigin: true,
                url: API_DOWNFILE + _name,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            };
            $http(config).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.reject(response.status);
            });
            return deferred.promise;
        }

    }
})();
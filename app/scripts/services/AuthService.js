(function() {
    'use strict';
    angular.module('printingApp')

    .factory('AuthService', AuthService);
    AuthService.$inject = [];

    function AuthService() {
        var service = {
            isLoggedIn: isLoggedIn
        }

        return service;

        /////

        function isLoggedIn() {
            var loginUser = JSON.parse(localStorage.loginUser);
            console.log(loginUser);
            return (loginUser) ? true : false;
        }
    }
})();